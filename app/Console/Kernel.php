<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Console\commands\weeklyEmail;
use App\Console\commands\MonthlyEmail;
use App\Console\commands\ThursdayEmail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        weeklyEmail::class,
        MonthlyEmail::class,
        ThursdayEmail::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('email:weekly')->weekly();

        $schedule->command('email:thursday')->weeklyOn(4, '9:00');

        $schedule->command('email:monthly')->monthlyOn(1, '9:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
