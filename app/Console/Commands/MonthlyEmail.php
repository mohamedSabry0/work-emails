<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\employee;
use App\email_content;

use Mail;
use App\Mail\SendEmail;

class MonthlyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:monthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Monthly Email To users list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $employees = employee::get();
        $email_content = email_content::where('type', 'month')->inRandomOrder()->first();

        foreach ($employees as $employee) {
            $data = [
                "email_title" => "Happy Monthly Email",
                "name" => $employee->name,
                "title" => $employee->title,
                "body" => $email_content->body,
                "signature" => $email_content->signature
            ];

           Mail::to($employee->email)->send(new SendEmail($data));

           $this->info('The emails are send successfully!');

        }
    
    }
}
