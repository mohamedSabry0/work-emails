<?php

namespace App\Http\Interfaces;


interface adminRepositoryInterface{


    /** Employee Section [all - add - update - delete]*/

    // All employees Data...
    public function allEmployees();
    


    // Add employee
    public function addEmployee($request);


    // update employee
    public function updateEmployee($request);


    // delete employee
    public function deleteEmployee($employee_id);



    /** Email Content Section [all - add - update - delete]*/

    // All employees Data...
    public function allEmailContents();


    // Add employee
    public function addEmailContent($request);


    // update employee
    public function updateEmailContent($request);


    // delete employee
    public function deleteEmailContent($email_content_id);




}