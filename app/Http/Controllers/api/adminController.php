<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\adminRepositoryInterface;

class adminController extends Controller
{

    /** Group of model as vars */
    protected $adminRepositoryInterface;

    /** Construct to handel inject models */
    public function __construct(adminRepositoryInterface $adminRepositoryInterface){
        $this->adminRepositoryInterface = $adminRepositoryInterface;
    }
 

    /** Employee Section [all - add - update - delete]*/

    // All employees Data...
    public function allEmployees(){
        return $this->adminRepositoryInterface->allEmployees();
    }
    


    // Add employee
    public function addEmployee(Request $request){
        return $this->adminRepositoryInterface->addEmployee($request);
    }


    // update employee
    public function updateEmployee(Request $request){
        return $this->adminRepositoryInterface->updateEmployee($request);
    }


    // delete employee
    public function deleteEmployee($employee_id){
        return $this->adminRepositoryInterface->deleteEmployee($employee_id);
    }



    /** Email Content Section [all - add - update - delete]*/

    // All employees Data...
    public function allEmailContents(){
        return $this->adminRepositoryInterface->allEmailContents();
    }


    // Add employee
    public function addEmailContent(Request $request){
        return $this->adminRepositoryInterface->addEmailContent($request);
    }


    // update employee
    public function updateEmailContent(Request $request){
        return $this->adminRepositoryInterface->updateEmailContent($request);
    }


    // delete employee
    public function deleteEmailContent($email_content_id){
        return $this->adminRepositoryInterface->deleteEmailContent($email_content_id);
    }
}
