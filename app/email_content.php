<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class email_content extends Model
{
    protected $fillable = [
        'body', 'signature', 'type',
    ];
}
