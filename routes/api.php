<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'admin'], function(){

    /** Employee Section [all - add - update - delete]*/
    Route::get('employee/all', 'adminController@allEmployees');
    Route::any('employee/add', 'adminController@addEmployee');
    Route::post('employee/update', 'adminController@updateEmployee');
    Route::get('employee/delete/{employee_id}', 'adminController@deleteEmployee');
    
   
    /** Email Content Section [all - add - update - delete]*/
    Route::get('email/all', 'adminController@allEmailContents');
    Route::post('email/add', 'adminController@addEmailContent');
    Route::post('email/update', 'adminController@updateEmailContent');
    Route::get('email/delete/{email_content_id}', 'adminController@deleteEmailContent');
    

});