@component('mail::message')

Hello {{$data['title']}} / {{$data['name']}},<br>

{{$data['body']}}<br>


Thanks,
{{$data['signature']}}<br>


